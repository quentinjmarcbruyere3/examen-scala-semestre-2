import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ParseArgumentsSpec extends AnyFlatSpec with Matchers {

  "parseArguments" should "return None for non-parsable arguments" in {
    val args = Array("--invalid-option", "scala")
    val result = Main.parseArguments(args)
    result should be(None)
  }

  it should "return Some(Config) with keyword specified" in {
    val args = Array("-w", "scala")
    val result = Main.parseArguments(args)
    result should be(Some(Config(keyword = "scala")))
  }

  it should "return Some(Config) with keyword and limit specified" in {
    val args = Array("-w", "scala", "-l", "10")
    val result = Main.parseArguments(args)
    result should be(Some(Config(keyword = "scala", limit = 10)))
  }
}

class TotalWordsSpec extends AnyFlatSpec with Matchers {

  "totalWords" should "return 0 for an empty list of pages" in {
    val pages = Seq.empty[WikiPage]
    val result = Main.totalWords(pages)
    result should be(0)
  }

  it should "return the total word count for a non-empty list of pages" in {
    val pages = Seq(
      WikiPage("Page 1", 100),
      WikiPage("Page 2", 200),
      WikiPage("Page 3", 150)
    )
    val result = Main.totalWords(pages)
    result should be(450)
  }
}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import play.api.libs.json.Json
import org.scalamock.scalatest.MockFactory

class MainSpec extends AnyFlatSpec with Matchers with MockFactory {

    // B. question 2
  "formatUrl" should "return the correct URL" in {
    val keyword = "Scala"
    val limit = 10
    val expectedUrl = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=&sroffset=0&list=search&srsearch=Scala&srlimit=10"

    val actualUrl = Main.formatUrl(keyword, limit)

    actualUrl shouldBe expectedUrl
  }

    // B. question 3
  "parseJson" should "return the list of page titles" in {
    val jsonStr =
      """
        |{
        |  "query": {
        |    "search": [
        |      {
        |        "title": "Scala (programming language)",
        |        "wordcount": 23
        |      },
        |      {
        |        "title": "Scala (TV series)",
        |        "wordcount": 10
        |      },
        |      {
        |        "title": "Scala (company)",
        |        "wordcount": 13
        |      }
        |    ]
        |  }
        |}
        |""".stripMargin
    
    val expectedTitles = Vector(WikiPage("Scala (programming language)", 23), WikiPage("Scala (TV series)", 10), WikiPage("Scala (company)", 13))

    val actualTitles = Main.parseJson(jsonStr)

    actualTitles shouldBe expectedTitles
  }

  // B. question 6
  "getPages" should "return Right with the response body" in {
    val mockHttp = mock[HttpRequest]
    val url = "https://example.com"
    val body = "Hello World"

    (mockHttp.getStatusCode(_: String)).expects(url).returning(200)
    (mockHttp.getBody(_: String)).expects(url).returning(body)
    val actualResult = Main.getPages(url)(mockHttp)

    actualResult shouldBe Right(body)
  }

  "getPages" should "return Left with the response code when code is not 200" in {
    val mockHttp = mock[HttpRequest]
    val url = "https://example.com"

    (mockHttp.getStatusCode(_: String)).expects(url).returning(400)
    val actualResult = Main.getPages(url)(mockHttp)

    actualResult shouldBe Left(400)
  }
} 
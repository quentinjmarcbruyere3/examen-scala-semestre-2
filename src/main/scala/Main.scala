import scopt.OParser
import scalaj.http.Http
import play.api.libs.json.{JsValue, Json}

case class Config(limit: Int = 10, keyword: String = "")
case class WikiPage(title: String, words: Int)

trait HttpRequest {

  /** Return the status code of a web page
    * @param url
    * @return
    */
  def getStatusCode(url: String): Int = Http(url).asString.code

  /** Return body of a web page
    * @param url
    * @return
    */
  def getBody(url: String): String = Http(url).asString.body
}

object HttpRequest extends HttpRequest {}

object Main extends App {

  /** Verify if arguments can be parse
    */
  parseArguments(args) match {
    case Some(config) => run(config)
    case _            => println("Unable to parse arguments")
  }

  /** Parse arguments with help, limit and keyword
    * @param args
    * @return
    */
  def parseArguments(args: Array[String]): Option[Config] = {
    val builder = OParser.builder[Config]
    val parser = {
      import builder._
      OParser.sequence(
        programName("WikiStats"),
        help('h', "help"),
        opt[Int]('l', "limit")
          .action((limit: Int, config: Config) => config.copy(limit = limit))
          .validate(limit =>
            if (limit > 0) success
            else failure("The limit must be greater than 0")
          )
          .text("Limit page"),
        opt[String]('w', "keyword").required
          .action((keyword: String, config: Config) =>
            config.copy(keyword = keyword)
          )
          .text("Keyword research")
      )
    }

    OParser.parse(parser, args, Config())
  }

  /** Run the program with arguments, it's print the title and the number of
    * words
    * @param config
    */
  def run(config: Config): Unit = {
    println(config)
    val url = formatUrl(config.keyword, config.limit)
    val pages = getPages(url)(http = HttpRequest)

    pages match {
      case Right(rawJson) =>
        parseJson(rawJson) match {
          case Nil =>
            println("Aucune page trouvée.")
          case wikiPages =>
            println(s"Nombre de pages trouvées : ${wikiPages.length}")
            wikiPages.foreach { page =>
              println(s"Titre : ${page.title}")
              println(s"Mots : ${page.words}")
              println()
            }
        }
      case Left(errorCode) =>
        println(s"Une erreur est survenue avec le code : $errorCode")
    }
  }

  /** Format the url with limit and keyword
    * @param keyword
    * @param limit
    * @return
    */
  def formatUrl(keyword: String, limit: Int): String = {
    "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=&sroffset=0&list=search&srsearch=" + keyword + "&srlimit=" + limit
  }

  /** Get page with the url and an httpRequest as implicit, it's return the body
    * of the request or it's code in case of error
    * @param url
    * @param http
    * @return
    */
  def getPages(url: String)(implicit http: HttpRequest): Either[Int, String] = {
    val result = http.getStatusCode(url)

    if (result == 200) {
      Right(http.getBody(url))
    } else {
      Left(result)
    }
  }

  /** Get elements title and wordcount on the json
    * @param rawJson
    * @return
    */
  def parseJson(rawJson: String): Seq[WikiPage] = {
    val json = Json.parse(rawJson)
    val results = (json \ "query" \ "search").as[Seq[JsValue]]

    results.flatMap { result =>
      for {
        title <- (result \ "title").asOpt[String]
        words <- (result \ "wordcount").asOpt[Int]
      } yield WikiPage(title, words)
    }
  }

  /** Count the total words
    * @param pages
    * @return
    */
  def totalWords(pages: Seq[WikiPage]): Int = {
    pages.foldLeft(0)((sum, page) => sum + page.words)
  }

}

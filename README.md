# Examen Scala - Semestre 2



## Consignes

Dans cet examen, nous allons utiliser une des APIs de wikipedia pour faire des statistiques sur les articles :
https://en.wikipedia.org/wiki/Special:ApiSandbox#action=query&format=json&prop=&list=search&srsearch=Scala&srlimit=10.

En allant sur le lien ci-dessus, dans l’onglet à gauche sur list=search, on peut choisir un mot clef à chercher
sur wikipedia (par exemple “Scala”), et en appuyant sur “Make Request”, on voit le résultat de l’appel API.

Dans cet examen, nous allons utiliser l’API pour récupérer les pages associées à un mot clef, traiter le résultat
pour le transformer en un objet scala, et analyser les informations des différentes pages.

## Lancer le linter

```commandLine
sbt scalafmt
```

## Lancer le programme

```commandLine
sbt
```

puis

```commandLine
run --limit 20 --keyword Scala
```

## Créer un rapport de code coverage

```commandLine
sbt coverageReport
```

Url: target/scala-2.12/scoverage-report/index.html

## Générer une documentation

```commandLine
sbt doc
```

Url: target/scala-2.12/api/index.html